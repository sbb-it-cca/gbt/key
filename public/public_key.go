package public

import (
	"crypto/ecdsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"errors"
	"fmt"
	"gopkg.in/resty.v1"
	"math/big"
	"os"
	"strings"

	"github.com/rs/zerolog/log"

	"gopkg.in/dgrijalva/jwt-go.v3"
)

type PublicKey interface {
	PublicEcdsaKey(userId string) (*ecdsa.PublicKey, error)
	VerifyPubSignature(userId, userSignature, expected string) error
	FindPublicKeyInfo(userId string) (*PublicKeyInfo, error)
	ToEcdsaKey(publicKeyInfo PublicKeyInfo) (*ecdsa.PublicKey, error)
}

type publicKeyImpl struct {
	urlSwisspass       string
	urlHandelsregister string
}

type EcdsaSignature struct {
	R *big.Int
	S *big.Int
}

const (
	SwissPassSuffix = "@swisspass.ch"
)

func (k *publicKeyImpl) PublicEcdsaKey(userId string) (*ecdsa.PublicKey, error) {
	var errReturn error
	var ecdsaKey *ecdsa.PublicKey

	if pubKey, err := k.public(userId); err != nil {
		errReturn = fmt.Errorf("error while loading public key: %v", err)
	} else {
		ecdsaKey, errReturn = k.convertToEcdsaKey(pubKey)
	}

	return ecdsaKey, errReturn
}

func (k *publicKeyImpl) public(userId string) (string, error) {
	var errReturn error
	var publicKeyInfo *PublicKeyInfo
	var publicKey string

	publicKeyInfo, errReturn = k.FindPublicKeyInfo(userId)

	if errReturn == nil {
		publicKey = publicKeyInfo.PublicKey
	}

	return publicKey, errReturn
}

func (k *publicKeyImpl) FindPublicKeyInfo(userId string) (*PublicKeyInfo, error) {
	var errReturn error
	var publicKeyInfo *PublicKeyInfo

	urlPklp := k.decideUrl(userId)

	if resp, err := resty.R().SetResult(&PublicKeyInfo{}).Get(urlPklp + userId); err != nil {
		errReturn = fmt.Errorf("unable to load public key from public key list provider: %s", err.Error())

	} else if resp.IsError() {
		errReturn = fmt.Errorf("entry not found")

	} else {
		publicKeyInfo = resp.Result().(*PublicKeyInfo)
	}

	return publicKeyInfo, errReturn
}

func (k *publicKeyImpl) ToEcdsaKey(publicKeyInfo PublicKeyInfo) (*ecdsa.PublicKey, error) {
	return k.convertToEcdsaKey(publicKeyInfo.PublicKey)
}

func (k *publicKeyImpl) convertToEcdsaKey(pubKey string) (*ecdsa.PublicKey, error) {
	complete := "-----BEGIN PUBLIC KEY-----\n" + pubKey + "\n-----END PUBLIC KEY-----"
	var (
		errReturn error
		ecdsaKey  *ecdsa.PublicKey
	)
	if ecdsaKey, errReturn = jwt.ParseECPublicKeyFromPEM([]byte(complete)); errReturn != nil {
		errReturn = fmt.Errorf("unable to parse ECDSA public key: %v", errReturn)
	}

	return ecdsaKey, errReturn
}

func (k *publicKeyImpl) decideUrl(userId string) string {

	if strings.HasSuffix(userId, SwissPassSuffix) {
		return k.urlSwisspass
	}

	return k.urlHandelsregister
}

func (k *publicKeyImpl) VerifyPubSignature(userId, userSignature, expected string) error {
	var errReturn error

	if pubKey, err := k.public(userId); err != nil {
		errReturn = fmt.Errorf("error retrieving public key: %s", err.Error())

	} else if decodedPublicKey, err := base64.StdEncoding.DecodeString(pubKey); err != nil {
		errReturn = fmt.Errorf("error decoding public key: %s", err.Error())

	} else if userPubKey, err := x509.ParsePKIXPublicKey(decodedPublicKey); err != nil {
		errReturn = fmt.Errorf("error parsing public key: %s", err.Error())

	} else if decodedUserSignature, err := base64.StdEncoding.DecodeString(userSignature); err != nil {
		errReturn = fmt.Errorf("error decoding user signature: %s", err.Error())

	} else if err := verifySignature(userPubKey.(*ecdsa.PublicKey), expected, decodedUserSignature); err != nil {
		errReturn = fmt.Errorf("signature is wrong!: %s", err)
	}

	return errReturn
}

func verifySignature(publicKey *ecdsa.PublicKey, message string, signature []byte) error {
	var esig EcdsaSignature
	var errReturn error
	digest := sha512.Sum384([]byte(message))

	if _, err := asn1.Unmarshal(signature, &esig); err != nil {
		errReturn = err
	} else if !ecdsa.Verify(publicKey, digest[:], esig.R, esig.S) {
		errReturn = errors.New(fmt.Sprintf("failed to verify authority signature"))
	}

	return errReturn
}

func initEnv(envVariable string) string {
	envValue := os.Getenv(envVariable)

	if len(envValue) == 0 {
		log.Fatal().Msgf("couldn't find env variable %s", envVariable)
	}

	return envValue
}

func New() *publicKeyImpl {
	return NewWithParams(initEnv("PKLP_SP_URL"), initEnv("PKLP_HR_URL"))
}

func NewWithParams(urlSwisspass, urlHandelsregister string) *publicKeyImpl {
	impl := publicKeyImpl{
		urlSwisspass:       urlSwisspass,
		urlHandelsregister: urlHandelsregister,
	}

	// check during compile that this implementation implements the interface PublicKey fully
	var _ PublicKey = &impl

	return &impl
}
