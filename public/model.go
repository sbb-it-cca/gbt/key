package public

type PublicKeyInfo struct {
	EntityId             string
	PublicKey            string
	LogoUrl              string
	Name                 string
	CertificateStatusUrl string
}
