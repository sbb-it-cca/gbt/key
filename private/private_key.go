package private

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha512"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"os"

	"github.com/rs/zerolog/log"

	"gopkg.in/dgrijalva/jwt-go.v3"
)

type PrivateKey interface {
	Sign(toSign string) (string, error)
	SignJwtToken(token jwt.Token) (string, error)
}

type privateKeyImpl struct {
	privateKey *ecdsa.PrivateKey
}

type EcdsaSignature struct {
	R *big.Int
	S *big.Int
}

const (
	EnvPrivateKeyPath = "PRIVATE_KEY_PATH"
)

func initPrivateKey() *ecdsa.PrivateKey {
	var errReturn error
	var privateKey *ecdsa.PrivateKey

	if privateKeyPath := os.Getenv(EnvPrivateKeyPath); len(privateKeyPath) == 0 {
		errReturn = fmt.Errorf("env variable '%s' must be specified", EnvPrivateKeyPath)

	} else if data, errRead := ioutil.ReadFile(privateKeyPath); errRead != nil {
		errReturn = fmt.Errorf("private key not found in path %s: %s", privateKeyPath, errRead.Error())

	} else {
		pemData, _ := pem.Decode(data)

		privateKey, errReturn = x509.ParseECPrivateKey(pemData.Bytes)

		if errReturn != nil {
			errReturn = fmt.Errorf("error parsing private key file in path %s: %s", privateKeyPath, errReturn.Error())
		}
	}

	if errReturn != nil {
		// do not start application
		log.Fatal().Msgf("private key couldn't be initialised: %s", errReturn.Error())
	}

	return privateKey
}

func (k *privateKeyImpl) Sign(toSign string) (string, error) {
	var authoritySignature string

	authoritySignatureHash := sha512.Sum384([]byte(toSign))
	authoritySignatureHashR, authoritySignatureHashS, err := ecdsa.Sign(rand.Reader, k.privateKey, authoritySignatureHash[:])

	if err == nil {
		base64authoritySignature, _ := asn1.Marshal(EcdsaSignature{authoritySignatureHashR, authoritySignatureHashS})
		authoritySignature = base64.StdEncoding.EncodeToString(base64authoritySignature)

	} else {
		err = errors.New(fmt.Sprintf("unable to sign authority signature: %s", err.Error()))
	}

	return authoritySignature, err
}

func (k *privateKeyImpl) SignJwtToken(token jwt.Token) (string, error) {
	return token.SignedString(k.privateKey)
}

func New() *privateKeyImpl {
	return NewWithParams(initPrivateKey())
}

func NewWithParams(privateKey *ecdsa.PrivateKey) *privateKeyImpl {
	impl := privateKeyImpl{
		privateKey: privateKey,
	}

	// check during compile that this implementation implements the interface PublicKey fully
	var _ PrivateKey = &impl

	return &impl
}
