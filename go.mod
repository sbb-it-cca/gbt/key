module gitlab.com/sbb-it-cca/gbt/key

go 1.12

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/rs/zerolog v1.14.3
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190514140710-3ec191127204 // indirect
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0
	gopkg.in/resty.v1 v1.12.0
)
